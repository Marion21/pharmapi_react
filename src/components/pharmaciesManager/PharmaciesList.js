import React from 'react';
import { Button, Table, Modal } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';

import PharmacyFormComponent from './PharmacyFormComponent';


export default function PharmaciesList({pharmacies, onFormSubmited, onDeletePharmacy}){

    const [modalVisible, setModalVisible] = React.useState();
    const [currentPharmacy, setCurrentPharmacy] = React.useState();
    
    const formSubmited = (formValues) => {
        setModalVisible(false);
        onFormSubmited(formValues);
    }

    const editPharmacy = (pharmacy) => {
        setModalVisible(true);
        setCurrentPharmacy(pharmacy);
      };

      const deletePharmacy = (pharmacyId) => {
        onDeletePharmacy(pharmacyId);
      };

    let columns = [
        {
          title: 'Nom',
          dataIndex: 'nom',
          key: 'nom',
        },
        {
            title: 'Ville',
            dataIndex: 'ville',
            key: 'ville',
        },
        {
            title: 'Quartier',
            dataIndex: 'quartier',
            key: 'quartier',
        },
        {
            title: 'De garde',
            dataIndex: 'garde',
            key: 'garde',
        },
        {
            title : 'Actions',
            dataIndex: 'action',
            key:'action',
            render: (_, record) => { 
                return (
                    <>
                    <Button icon={<EditOutlined />} onClick={() => editPharmacy(record)}>Modifier</Button>{' '}
                    <Button icon={<DeleteOutlined />} danger onClick={() => deletePharmacy(record.id)}>Supprimer</Button>
                    </>
                )
            },
        }
    ]

    return (
        <>
            <Table columns={columns} dataSource={pharmacies} pagination={false} rowKey={(p) => p.id}/> 
            

            <Modal title="Modifier Pharmacie" 
                visible={modalVisible} 
                onOk={() =>setModalVisible(false)}
                onCancel={() =>setModalVisible(false)} 
                footer={null} 
            >
                <PharmacyFormComponent initialValues={currentPharmacy} onFormSubmited={formSubmited} />

            </Modal>
            
        </>
    )
    
}
