import React from 'react';
import { Switch, PageHeader, Modal, Button } from 'antd';
import { PlusCircleOutlined } from '@ant-design/icons';

import PharmacyFormComponent from './PharmacyFormComponent';



export default function Header({onFormSubmited, onGardeChange}){ 

    const [modalVisible, setModalVisible] = React.useState();
    
    const formSubmited = (formValues) => {
        setModalVisible(false);
        onFormSubmited(formValues);
    }

    return(
        <>
            <PageHeader
                className="site-page-header"
                title="PharmAPI"
                subTitle="Gestion de pharmacies"
                extra={[
                    <>
                        Afficher {' '}
                        <Switch checkedChildren="De garde" unCheckedChildren="Toutes" onChange={onGardeChange} />
                    </>, 
                    <Button icon={<PlusCircleOutlined />} key="createPharmacyButton"  type="primary" onClick={() => setModalVisible(true)}>Créer une pharmacie</Button>,
                ]}
            />

            <Modal title="Nouvelle Pharmacie" 
                visible={modalVisible} 
                onOk={() =>setModalVisible(false)}
                onCancel={() =>setModalVisible(false)} 
                footer={null} 
            >
                <PharmacyFormComponent onFormSubmited={formSubmited} />

            </Modal>
        </>
    )
    
}
