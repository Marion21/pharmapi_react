import React from 'react';
import { notification } from 'antd';
import Header from './Header';
import axios from 'axios'; 
import PharmaciesList from './PharmaciesList';


export default function PharmaciesManager(){ 

    const [pharmacies, setPharmacies] = React.useState([]);

    // Au début, on charge les pharmacies
    React.useEffect(() => {
		getPharmacies(false);
	}, []);

    /**
     * Lister les pharmacies
     * 
     * @param {*} filterGarde 
     */
    const getPharmacies = (filterGarde) => {
        let route = 'pharma';
        if(filterGarde)
            route = 'pharma-garde'

        axios.get('https://cherry-tart-07032.herokuapp.com/'+route)
        .then(res => {
            setPharmacies(res.data)
        })
    }

    /**
     * Création de la pharmacie
     * 
     * @param {*} formValues 
     */
    const createPharmacie = (formValues) => {       
        axios.post('https://cherry-tart-07032.herokuapp.com/pharma', formValues)
                .then((response) => {
                    var id = parseInt(response.data.substr(26));
                    formValues.id = id;

                   let newPharmacyList = pharmacies.map(p => p);
                   newPharmacyList.push(formValues)
                   setPharmacies(newPharmacyList)

                    notification['success']({
                        message: 'Pharmacie créée avec succès',
                        description: 'Nouvelle pharmacie créée avec l\'ID ' + id,
                      });

                }, (error) => {
                    notification['error']({
                        message: 'Erreur !',
                        description: error + '',
                      });
                });   
    }

    /**
     * Mise à jour de la pharmacie
     * 
     * @param {*} formValues 
     */
    const updatePharmacie = (formValues) => {

        axios.put('https://cherry-tart-07032.herokuapp.com/pharma/'+formValues.id, formValues)
                .then((response) => 
            {
                var newPharmaciesList = pharmacies
                .map(pharmacy => {
                    if(pharmacy.id === formValues.id){
                        return formValues;
                    }else{
                        return pharmacy;
                    }
                });

                setPharmacies(newPharmaciesList)
                notification['success']({
                    message: 'Good !',
                    description: 'Pharmacie modifiée avec succès',
                    });

            }, (error) => {
                    notification['error']({
                        message: 'Erreur !',
                        description: error + '',
                      });
               
            }); 
    }

    /**
     * Supprimer la pharmacie
     * 
     * @param {*} id 
     */
    const deletePharmacie = (id) => {

        var newPharmaciesList = pharmacies.filter(pharmacy => pharmacy.id !== id);
        
        axios.delete('https://cherry-tart-07032.herokuapp.com/pharma/'+id)
        .then((response) => {    
            setPharmacies(newPharmaciesList)
            notification['success']({
                message: 'Good !',
                description: 'Pharmacie supprimée avec succès',
              });
        }, (error) => {
            notification['error']({
                message: 'Erreur !',
                description: error + '',
                });
        });  
    }

    return(
        <>
            <Header onGardeChange={getPharmacies} onFormSubmited={createPharmacie} />
            
            <PharmaciesList pharmacies={pharmacies} onFormSubmited={updatePharmacie} onDeletePharmacy={deletePharmacie} />
        </>
    )
    
}
