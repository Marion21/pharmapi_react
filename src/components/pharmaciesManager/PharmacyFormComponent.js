import React from 'react';
import { Form, Select, Input, Button} from 'antd';

const { Option } = Select;

const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
    };

const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
};  

export default function PharmacyFormComponent({initialValues, onFormSubmited}) {

    const [form] = Form.useForm();
    form.setFieldsValue(initialValues);

    // Reset fields + send form
    const submitForm = (formValues) => {
        form.resetFields();
        onFormSubmited(formValues);
    }

    return (

        <Form form={form} 
        {...layout}
        name="ajout"
        onFinish={submitForm}
    >
        <Form.Item 
        name="id"
        hidden
        >
        <Input />
        </Form.Item>

        <Form.Item 
        label="Nom de la pharmacie"
        name="nom"
        >
        <Input />
        </Form.Item>

        <Form.Item
        label="Quartier"
        name="quartier"
        >
        <Input />
        </Form.Item>

        <Form.Item
        label="Ville"
        name="ville"
        >
        <Input />
        </Form.Item>

        <Form.Item
        label="Jour de garde"
        name="garde"
        >
            <Select optionFilterProp="children">
                <Option value="lundi">Lundi</Option>
                <Option value="mardi">Mardi</Option>
                <Option value="mercredi">Mercredi</Option>
                <Option value="jeudi">Jeudi</Option>
                <Option value="vendredi">Vendredi</Option>
                <Option value="samedi">Samedi</Option>
                <Option value="dimanche">Dimanche</Option>
            </Select>
        </Form.Item>

        <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
                Envoyer
            </Button>
        </Form.Item>
    </Form> 

    )
}