import React, { Component } from 'react';

import './App.css';

import PharmaciesManager from './components/pharmaciesManager/PharmaciesManager'


class App extends Component {

  render(){
    return (
      <>
        <PharmaciesManager />
      </>
    );
  }
}

export default App;
