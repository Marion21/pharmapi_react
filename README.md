Contexte du projet
Suite à votre excellente prestation de Pharmapi en Symfony. Vos etes chargé de développer l'application web permettant de :

consulter la liste des pharmacies
consulter la liste des pharmacies de garde
ajouter, éditer, supprimer des pharmacies
le site doit être une PWA
Créer les maquettes les plus attrayantes possibles, utiliser Bootstrap

Utiliser Axios pour les requêtes à l'api.
